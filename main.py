import kivy
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
import sqlite3
import datetime
import Pyro4
import serpent
import os
from kivy import platform
if platform == "android":
    from android.permissions import request_permissions, Permission
    request_permissions([Permission.CAMERA, Permission.WRITE_EXTERNAL_STORAGE, 
Permission.READ_EXTERNAL_STORAGE])

if (os.path.isdir('/storage/emulated/0/Download/securityCameraPictures') == False):
    os.mkdir("/storage/emulated/0/Download/securityCameraPictures")
kivy.require('2.2.1')


#Client
ns = Pyro4.locateNS()
uri = ns.lookup('server')
server= Pyro4.Proxy(uri)

def download(file_name):
    output_file = open(file_name, "wb")
    output_file.write(serpent.tobytes(server.download(file_name)))
    output_file.close()
    print ("Downloaded file: {}".format(file_name))

today = datetime.datetime.now()
date_time = today.strftime("%m%d%Y")

def writeTofile(data, filename):
    # Convert binary data to proper format and write it on Hard Disk
    with open(filename, 'wb') as file:
        file.write(data)
    #print("Stored blob data into: ", filename, "\n")

def readBlobData(MotId,itr):
    try:
        sqliteConnection = sqlite3.connect('SQLite_Python_Image.db')
        cursor = sqliteConnection.cursor()
        #print("Connected to SQLite")

        sql_fetch_blob_query = """SELECT * from Motion where id = ?"""
        cursor.execute(sql_fetch_blob_query, (MotId,))
        record = cursor.fetchall()
        for row in record:
            print("Id = ", row[0])
            photo = row[1]

            converted = "% s" % itr
            photoPath = "/storage/emulated/0/Download/securityCameraPictures/"+ date_time + "-" + converted +".jpg"
            writeTofile(photo, photoPath)

        cursor.close()

    except sqlite3.Error as error:
        print("Failed to read blob data from sqlite table", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            #print("sqlite connection is closed")


class MyRoot(BoxLayout):
    
    def __init__(self):
        super(MyRoot, self).__init__()
    
    def security(self):
        download('SQLite_Python_Image.db')
        n = int(server.value())
        itr=1
        itrate=1
        for i in range(0, n):
            readBlobData(itr,itrate)
            itr+=1
            itrate+=1
        self.sec_label.text = "done!"

class Ran(App):
	def build(self):
		return MyRoot()

Ran = Ran()
Ran.run()
